package net.justugh.sc.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SafeLocation {

    private final int x, y, z;

}
