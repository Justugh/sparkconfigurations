package net.justugh.sc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.justugh.sc.config.impl.MapConfig;
import net.justugh.sc.util.SafeLocation;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;

public class SparkConfigurations extends JavaPlugin {

    private MapConfig exampleConfig;

    @Override
    public void onEnable() {
        File mapConfigFile = new File("example_map.json");

        if(mapConfigFile.exists()) {
            exampleConfig = (MapConfig) MapConfig.load(mapConfigFile, MapConfig.class);
        } else {
            exampleConfig = new MapConfig(new File("example_map.json"));
            // Set debug values
            exampleConfig.setName("Example");
            exampleConfig.setAuthor("Example Author");
            exampleConfig.setCornerA(new Vector(0, 0, 0));
            exampleConfig.setCornerB(new Vector(1, 1, 1));
            exampleConfig.setLobbySpawn(new SafeLocation(0, 1, 0));
            exampleConfig.setSpawnPoints(Lists.newArrayList(new SafeLocation(1, 3, 3)));
            HashMap<String, String> customData = Maps.newHashMap();
            customData.put("ExampleData", "Value");
            exampleConfig.setCustomData(customData);
            exampleConfig.save();
        }

        displayExample();
    }

    @Override
    public void onDisable() {

    }

    private void displayExample() {
        getLogger().log(Level.INFO, "Example Info:");
        getLogger().log(Level.INFO, "Map Name:" + exampleConfig.getName());
        getLogger().log(Level.INFO, "Map Author:" + exampleConfig.getAuthor());
        getLogger().log(Level.INFO, "Corner A:" + exampleConfig.getCornerA().toString());
        getLogger().log(Level.INFO, "Corner B:" + exampleConfig.getCornerB().toString());
        getLogger().log(Level.INFO, "Lobby Spawn: " + exampleConfig.getLobbySpawn().toString());
        getLogger().log(Level.INFO, "Spawn Locations:");
        exampleConfig.getSpawnPoints().forEach(loc -> getLogger().log(Level.INFO, loc.toString()));
        getLogger().log(Level.INFO, "Custom Data:");
        exampleConfig.getCustomData().forEach((key, value) -> getLogger().log(Level.INFO, key + " " + value));
        getLogger().log(Level.INFO, "--- Info Finished ---");

    }

}
