package net.justugh.sc.config;

import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;

@Getter
@NoArgsConstructor
public class SparkConfig {

    private File configFile;

    public SparkConfig(File configFile) {
        this.configFile = configFile;
    }

    public void save() {
        try {
            FileUtils.writeStringToFile(configFile, new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(this), Charset.defaultCharset());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static SparkConfig load(File file, Class<? extends SparkConfig> configClass) {
        if(!file.exists()) {
            return null;
        }

        try {
            return new GsonBuilder().create().fromJson(new FileReader(file), configClass);
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }

        return null;
    }

}
