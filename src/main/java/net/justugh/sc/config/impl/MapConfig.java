package net.justugh.sc.config.impl;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.justugh.sc.config.SparkConfig;
import net.justugh.sc.util.SafeLocation;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MapConfig extends SparkConfig {

    private String name, author;
    private Vector cornerA, cornerB;
    private SafeLocation lobbySpawn;
    private List<SafeLocation> spawnPoints;

    private HashMap<String, String> customData;

    public MapConfig(File configFile) {
        super(configFile);
    }

}
